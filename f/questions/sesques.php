<?php
require_once "../functions.php";
if (!isset($_GET['s'])) {
  header('location: ./');
}
$sess_id = $_GET['s'];
$ques = new Question();
$ques->__set('sessionid', $sess_id);
$quesList = $ques->getSessAllQuestions();
//var_dump($quesList);
$data = array();
$event = new Event();
if (!empty($quesList)) {
  $i = 0;
  foreach ($quesList as $c) {
    $data[$i]['First Name'] = $c['firstname'];
    $data[$i]['Last Name'] = $c['lastname'];
    $data[$i]['E-mail ID'] = $c['emailid'];
    $data[$i]['Question'] = $c['question'];
    $data[$i]['Asked at'] = $c['asked_at'];

    $i++;
  }

  $filename = "Questions.xls";
  header("Content-Type: application/vnd.ms-excel");
  header("Content-Disposition: attachment; filename=\"$filename\"");
  ExportFile($data);
}
