<?php
require_once '../functions.php';
require_once 'header.php';
?>

<body>
  <div class="container-fluid">
    <div class="row mt-5">
      <div class="col-8 col-md-6 offset-2 offset-md-3 ">
        <div class="form-wrapper">
          <h6>Select Session</h6>
          <div id="sessions" class="mt-2">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>Track 01 - <a href="questions.php?sess=21ef43cd7b98f92132a1a06fc8771597df6050634c54ba4ff95c73eb414b522b"><b>Day1 - Ortho NE</b></a></td>
                  <td width="50"><a href="sesques.php?s=21ef43cd7b98f92132a1a06fc8771597df6050634c54ba4ff95c73eb414b522b"><i class="fas fa-download"></i></a></td>
                </tr>
                <tr>
                <tr>
                  <td>Track 02 - <a href="questions.php?sess=f768722f6202e374317959a31f83579e3a76b91a0e2e159b42da8985960862f4"><b>Day1 - Ortho SW</b></a></td>
                  <td width="50"><a href="sesques.php?s=f768722f6202e374317959a31f83579e3a76b91a0e2e159b42da8985960862f4"><i class="fas fa-download"></i></a></td>
                </tr>
                <tr>
                  <td>Track 03 - <a href="questions.php?sess=04dddd55a0f9364c24f24c8af6a033a7157d0ab2be4a206ede6b2e8341ece1e7"><b>Day1 - Gynaec</b></a></td>
                  <td width="50"><a href="sesques.php?s=04dddd55a0f9364c24f24c8af6a033a7157d0ab2be4a206ede6b2e8341ece1e7"><i class="fas fa-download"></i></a></td>
                </tr>
                <tr>
                  <td>Track 04 - <a href="questions.php?sess=0a16ff8053686d8b3939eac4b7baa940f1e84e01af9451dcba3a5adb4956a9a7"><b>Day2 - Ortho NE</b></a></td>
                  <td width="50"><a href="sesques.php?s=0a16ff8053686d8b3939eac4b7baa940f1e84e01af9451dcba3a5adb4956a9a7"><i class="fas fa-download"></i></a></td>
                </tr>
                <tr>
                  <td>Track 05 - <a href="questions.php?sess=4b03177571eb4ef4d566673b10aff638c6e65b9532c2c0bb08de8d02e3bcd73f"><b>Day2 - Ortho SW</b></a></td>
                  <td width="50"><a href="sesques.php?s=4b03177571eb4ef4d566673b10aff638c6e65b9532c2c0bb08de8d02e3bcd73f"><i class="fas fa-download"></i></a></td>
                </tr>
                <tr>
                  <td>Track 06 - <a href="questions.php?sess=ea3af3eae286bc311fa31910045b08e8882002d8d0bca958e790bfd0956f5b2a"><b>Day2 - Gynaec</b></a></td>
                  <td width="50"><a href="sesques.php?s=ea3af3eae286bc311fa31910045b08e8882002d8d0bca958e790bfd0956f5b2a"><i class="fas fa-download"></i></a></td>
                </tr>
                <tr>
                  <td>Track 07 - <a href="questions.php?sess=60d8a4c39480c0ae8b549e7c9840b6344d8ec297585a79add6a002c46b83929b"><b>Day3 - Gynaec</b></a></td>
                  <td width="50"><a href="sesques.php?s=60d8a4c39480c0ae8b549e7c9840b6344d8ec297585a79add6a002c46b83929b"><i class="fas fa-download"></i></a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php
  require_once 'footer.php';
  ?>