<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$emailid = '';

if (isset($_POST['loginuser-btn'])) {
  if (empty($_POST['emailid'])) {
    $errors['email'] = 'Email ID is required';
  }

  $emailid = $_POST['emailid'];

  if (count($errors) == 0) {
    $user = new User();
    $user->__set('emailid', $emailid);
    $login = $user->userLogin();
    //var_dump($login);
    $reg_status = $login['status'];
    if ($reg_status == "error") {
      $errors['login'] = $login['message'];
    }
  }
}
?>

<?php require_once 'header.php';  ?>

<div class="container-fluid">
  <div class="row pt-4 mb-1">
    <div class="col-12 col-md-6 col-lg-6 offset-lg-1">
      <img src="assets/img/login-banner.png" class="img-fluid" alt="">
      <!--  <div class="row p-3">
        <div class="col-12 col-md-3 offset-md-1 text-center">
          <a href="register.php"><img src="assets/img/register-now.png" class="img-fluid" alt=""></a>
        </div>
        <div class="col-12 col-md-3 offset-md-2 text-center">
          <a class="agenda" href="assets/resources/conf-agenda.pdf"><img src="assets/img/agenda.png" class="img-fluid" alt=""></a>
        </div>
      </div> -->
      <div class="right-area-wrapper">
        <div class="row mt-3">
          <div class="col-12">
            If already registered, please login here:
            <?php
            if (count($errors) > 0) : ?>
              <div class="alert alert-danger alert-msg">
                <ul class="list-unstyled">
                  <?php foreach ($errors as $error) : ?>
                    <li>
                      <?php echo $error; ?>
                    </li>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endif; ?>
            <form action="" method="post">
              <div class="form-group">
                <input type="text" name="emailid" id="emailid" class="input" placeholder="Enter your Email ID" value="<?= $emailid ?>">
              </div>
              <div class="form-group">
                <input type="submit" name="loginuser-btn" id="btnLogin" class="form-submit btn-login" value="Login" />
              </div>
            </form>

          </div>
        </div>


      </div>
    </div>
    <div class="col-12 col-md-6 col-lg-5 text-center">
      <img src="assets/img/login-banner-02.png" class="img-fluid" alt="">
      <div class="mt-3 text-center">
        <h5>For assistance:<br>
          <i class="fas fa-phone-square-alt"></i> +917314-855-655</h5>
      </div>
    </div>
  </div>
  <!--  <div class="row p-3">
    <div class="col-12 col-md-3 text-center">
      <a href="register.php"><img src="assets/img/register-now.png" class="img-fluid" alt=""></a>
    </div>
    <div class="col-12 col-md-3 offset-md-1 text-center">
      <a class="agenda" href="assets/resources/conf-agenda.pdf"><img src="assets/img/agenda.png" class="img-fluid" alt=""></a>
    </div>
  </div> -->
</div>

<script src="//code.jquery.com/jquery-latest.js"></script>
<script src="assets/js/mag-popup.js"></script>
<script type="text/javascript" src="assets/js/jquery.syotimer.min.js"></script>
<script>
  $(document).ready(function() {
    $('.agenda').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,

      fixedContentPos: false
    });
    $('#countdown').syotimer({
      year: 2020,
      month: 11,
      day: 21,
      hour: 10,
      minute: 00,
      timeZone: 0,
      ignoreTransferTime: true,
      layout: 'dhms',
      afterDeadline: function() {
        $('#timer').fadeOut();
      }

    });

  });
</script>
<?php require_once 'ga.php';  ?>
<?php require_once 'footer.php';  ?>