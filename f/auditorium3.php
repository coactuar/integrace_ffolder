<?php
require_once "logincheck.php";
require_once "functions.php";

$audi_id = 'd92d15f91a4b105183d3665531c7fc7a67c98176e22bcc96c3dc91aee56c6334';
$audi = new Auditorium();
$audi->__set('audi_id', $audi_id);
$a = $audi->getEntryStatus();
$entry = $a[0]['entry'];
if (!$entry) {
    header('location: lobby.php');
}
$curr_room = 'auditorium3';
$webcastUrl = '#';

$sess_id = 0;
if (isset($_GET['ses'])) {
    $sess_id = $_GET['ses'];
    $sess = new Session();
    $sess->__set('session_id', $sess_id);
    $curr_sess = $sess->getSession();
    if ((empty($curr_sess)) || (!$curr_sess[0]['launch_status'])) {
        header('location: auditorium3.php');
    }
    $webcastUrl = $sess->getWebcastSessionURL();
    //$webcastUrl .= '?autoplay=1';
} else {
    //$webcastUrl .= '&loop=1';
}
?>
<?php require_once 'audi-header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/auditorium03_area.png">
            <?php if ($sess_id != '0') { ?>
                <div id="webcast-area">
                    <!--<a id="goFS" href="#" class="fs">Fullscreen</a>
                    <iframe src="<?= $webcastUrl ?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="width:100%;height:100%;"></iframe>-->
                    <div id="player"></div>
                </div>
            <?php } ?>
            <!-- <a href="#" id="audiFenza">
                <div class="indicator d-4"></div>
            </a>
            <a href="#" id="audiDubagestSR">
                <div class="indicator d-4"></div>
            </a>
            <a href="#" id="audiMilical">
                <div class="indicator d-4"></div>
            </a>
            <a href="#" id="audiMumfer">
                <div class="indicator d-4"></div>
            </a> -->
        </div>
        <div id="audiAgenda">
            <a id="showaudiAgenda" href="assets/resources/gynaec-agenda.pdf" class="showpdf"><i class="far fa-list-alt"></i>Agenda</a>
        </div>
        <?php
        if ($sess_id != '0') {
        ?>

            <div id="ask-ques">
                <a href="#" id="askques"><i class="fas fa-question-circle"></i>Ask Ques</a>
            </div>
            <div id="take-poll">
                <a href="#" id="takepoll"><i class="fas fa-poll"></i>Take Poll</a>
            </div>
            <div class="panel ques">
                <div class="panel-heading">
                    Ask A Question
                    <a href="#" class="close" id="close_ques"><i class="fas fa-times"></i></a>
                </div>
                <div class="panel-content">
                    <div id="ques-message" style="display:none;"></div>
                    <form>
                        <div class="form-group">
                            <textarea class="input" rows="6" name="userques" id="userques" required></textarea>
                        </div>
                        <div class="form-group">
                            <button type="button" name="send_sesques" data-ses="<?= $sess_id ?>" data-user="<?= $userid ?>" class="send_sesques btn btn-sm btn-primary btn-sendmsg">Submit Question</button>
                        </div>
                    </form>
                    <div id="askedQues">
                        <div id="quesList" class="scroll">

                        </div>

                    </div>
                </div>

            </div>
            <div class="panel poll">
                <div class="panel-heading">
                    Take Poll
                    <a href="#" class="close" id="close_poll"><i class="fas fa-times"></i></a>
                </div>
                <div class="panel-content">
                    <div id="poll-message" style="display:none;"></div>
                    <div id="currpollid" style="display:none;">0</div>
                    <div id="currpoll" style="display:none;">

                    </div>
                    <div id="currpollresults" style="display:none">

                    </div>
                </div>

            </div>
        <?php
        }
        ?>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
</div>



<?php require_once "commons.php" ?>
<?php require_once "scripts.php" ?>
<?php require_once "audi-common.php" ?>
<?php require_once "audi-script.php" ?>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>
  <script>
    var player = new Clappr.Player(
    {
        source: "<?= $webcastUrl ?>",      
        parentId: "#player",
        plugins: [LevelSelector],
        levelSelectorConfig: {
          title: 'Quality',
          labels: {
              3: '1080p',              
              2: '720p',              
              1: '480p', // 500kbps
              0: '360p', // 240kbps
          },
          labelCallback: function(playbackLevel, customLabel) {
              return customLabel;// + playbackLevel.level.height+'p'; // High 720p
          }
        },
        //poster: "img/poster.jpg",
        width: "100%",
        height: "100%",
        
    });
    
    player.play();
</script>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>