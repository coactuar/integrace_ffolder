<?php
set_time_limit(0);
require_once "functions.php";

$start_date   = date('Y/m/d H:i:s');
echo "Started at: ".$start_date ."<br><br>";

// INIT + RUN SETTINGS
set_time_limit(0);
$each = 4;
$pause = 5;
$mail_msg = '';

$offset = 0;
$user = new User();
$all = $user->getUserCount();
$mail_sent = 0;
$mail_notsent = 0;

for($i=0; $i<=$all; $i+=$each){
    echo $i.'<br>';
    $user->__set('limit',$each);
    $userList = $user->getAllUsers($i);
    //var_dump($userList);
    
      foreach($userList as $c){
        $name = $c['firstname'].' '.$c['lastname'];
        $email = $c['emailid'];
        //echo $name . ': '. $email.'<br>';
        $mailer = new Mail();
        $mailer->__set('emailto', $email);
        $mailer->__set('name', $name);
        $status = $mailer->sendEmail();
        if($status){
            $mail_sent++;
            $mail_msg .= "<br>Mail sent to " . $email.'<br>';
        }
        else{
            $mail_notsent++;
            $mail_msg .= "<br>Mail could not be sent to " . $email.'<br>';
        }
      }
    //echo 'Taking '.$pause.'sec rest <br>';
    sleep($pause);
}
//echo $mail_msg.'<br>';

echo "Mail Sent to: " . $mail_sent ."<br>";
echo "Mail not Sent to: " . $mail_notsent ."<br>";
echo "Total Members: " . $all."<br><br>";

$end_date   = date('Y/m/d H:i:s');
echo "Finished at: ".$end_date ."<br><br>";

?>