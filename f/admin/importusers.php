<?php
require_once '../functions.php';
define('SITE_ROOT', realpath(dirname(__FILE__)));

$allowedFileType = ['application/vnd.ms-excel', 'text/xls', 'text/xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

$message = 'Error updating users';
if (in_array($_FILES["file"]["type"], $allowedFileType)) {

  $targetPath = 'uploads/' . $_FILES['file']['name'];
  //echo $targetPath;
  $status = move_uploaded_file($_FILES['file']['tmp_name'],  SITE_ROOT . '/' . $targetPath);

  $file = $targetPath;
  $handle = fopen($file, "r");
  $c = 0;
  $n = 0;
  $d = 0;
  $nv=0;
  
  $d_rec = '';
  $nv_rec = '';
  
  while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
    $div = $filesop[0];
    $fname = $filesop[1];
    $lname = $filesop[2];
    $loc = $filesop[3];
    $mobile = $filesop[4];
    $emailid = $filesop[5];

    if (($emailid == '') || ($fname == '')) {
      //do nothing
      $message = "CSV File is missing data.";
      $nv = $nv + 1;
      $nv_rec .= $email.', ';
    } 
    else {
        $user = new User();
        $newuser = new User();
        $newuser->__set('firstname', $fname);
        $newuser->__set('lastname', $lname);
        $newuser->__set('emailid', $emailid);
        $newuser->__set('mobilenum', $mobile);
        $newuser->__set('location', $loc);
        $newuser->__set('division', $div);

        $add = $newuser->addUser();
        //var_dump($add);
        $reg_status = $add['status'];
        if ($reg_status != "success") {
            $d = $d + 1;
        }
        else{
            $n = $n+1;
        }
        
      }
    
    $c = $c + 1;
  }
  $message = $c . " records accessed. ";
  $message .= $n . " users imported in database. ";
  if ($d > 0) {
    $message .= $d . " duplicate records found.";
  }
  if ($nv > 0) {
    $message .= $nv . " invalid records found.";
  } 
  
  if ($d_rec !='') {
    $message .= '<br>Duplicate Records: ' . $d_rec;
  }
  if ($nv_rec !='') {
    $message .= '<br>Invalid Records: ' . $nv_rec;
  } 
  
} 
else {
  $message = "Invalid File Type. Upload CSV File.";
}
echo $message;

