<?php
require_once "logincheck.php";
$curr_room = 'lobby';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/lobby.jpg">

            <div id="leftBanner">
                <div id="leftCar" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="assets/img/logo-ortho.png" alt="Ortho">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="assets/img/logo-gynaec.png" alt="Gynaec">
                        </div>
                    </div>
                </div>
            </div>
            <div id="rightBanner">
                <div id="rightCar" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item ">
                            <img class="d-block w-100" src="assets/img/logo-ortho.png" alt="Ortho">
                        </div>
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="assets/img/logo-gynaec.png" alt="Gynaec">
                        </div>
                    </div>
                </div>
            </div>


            <div id="lobbyVideo">
                <div id="player"></div>
            </div>
            <a href="auditorium1.php" id="enterAudi1">
                <div class="indicator d-6"></div>
            </a>
            <a href="auditorium2.php" id="enterAudi2">
                <div class="indicator d-6"></div>
            </a>
            <a href="auditorium3.php" id="enterAudi3">
                <div class="indicator d-6"></div>
            </a>
            <a href="assets/resources/top_fso.pdf" class="showpdf" id="topFSO">
                <div class="indicator d-4"></div>
            </a>
            <a href="assets/resources/fg_fso.pdf" class="showpdf" id="fgFSO">
                <div class="indicator d-4"></div>
            </a>
            <a href="assets/resources/top_dsm.pdf" class="showpdf" id="topDSM">
                <div class="indicator d-4"></div>
            </a>
            <a href="assets/resources/fg_dsm.pdf" class="showpdf" id="fgDSM">
                <div class="indicator d-4"></div>
            </a>
            <a href="assets/resources/fg_zsm.pdf" class="showpdf" id="fgZSM">
                <div class="indicator d-4"></div>
            </a>

            <a href="assets/resources/mumfermax.pdf" class="showpdf" id="mumfermax">
                <div class="indicator d-4"></div>
            </a>
            <a href="assets/resources/milical.pdf" class="showpdf" id="milical">
                <div class="indicator d-4"></div>
            </a>
            <a href="assets/resources/dubagest.pdf" class="showpdf" id="dubagest">
                <div class="indicator d-4"></div>
            </a>
            <a href="assets/resources/fenza.pdf" class="showpdf" id="fenza">
                <div class="indicator d-4"></div>
            </a>
            <a href="assets/resources/fenzawash.pdf" class="showpdf" id="fenzawash">
                <div class="indicator d-4"></div>
            </a>

            <a href="#" id="helpDesk" class="show_talktous" data-from="<?php echo $_SESSION['userid']; ?>">
                <div class="indicator d-6"></div>
            </a>

            <a href="assets/resources/confagenda.pdf" class="showpdf" id="showAgenda">
                <div class="indicator d-4"></div>
            </a>
            <a href="https://integrace.sharepoint.com/sites/HRValue/Pages/IntegraceForm.aspx" target="_blank" id="showIntValue">
                <div class="indicator d-4"></div>
            </a>
            <a href="#" id="showIntIcon">
                <div class="indicator d-4"></div>
            </a>

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
    <div class="modal fade" id="icons" tabindex="-1" role="dialog" aria-labelledby="iconsLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="iconsLabel">Integrace Icon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <ul class="popuplist">
                        <li>
                            <a class="viewpoppdf" href="assets/resources/ortho_2.pdf">Ortho - 2nd Quarter</a>
                        </li>
                        <li>
                            <a class="viewpoppdf" href="assets/resources/ortho_3.pdf">Ortho - 3rd Quarter </a>
                        </li>
                        <li>
                            <a class="viewpoppdf" href="assets/resources/gynaec_2.pdf">Gynae - 2nd Quarter</a>
                        </li>
                        <li>
                            <a class="viewpoppdf" href="assets/resources/gynaec_3.pdf">Gynae - 3rd Quarter </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once "scripts.php" ?>
<script>
    $(function() {
        $('.carousel').carousel({
            interval: 2000
        });
        $('#showIntIcon').on('click', function() {
            $('#icons').modal('show');
        });
    });
</script>
<script>
    var player = new Clappr.Player({
        source: "https://d38233lepn6k1s.cloudfront.net/out/v1/c93c714e139f4640939b493d754f7a39/4553131ab95f4d748725f284186e6605/57c32c311a6e4f23bdd4bd4d63d426a1/index.m3u8",
        parentId: "#player",
        width: "100%",
        height: "100%",
        loop: true,
        autoplay: true

    });

    player.play();
</script>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>