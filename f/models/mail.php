<?php

class Mail
{

  public $emailto;
  public $name;
  public $subject = 'Thank you for Registering';
  public $message;

  const MAILHOST = 'smtp.gmail.com';
  const MAILUSER = 'support@coact.co.in';
  const MAILPASS = 'coact2020';
  const MAILEMAIL = 'support@coact.co.in';
  const MAILNAME = 'eCBM3 Registration';

  function __construct()
  {
    //require_once __ROOT__ . '\assets\vendor\autoload.php';
  }


  public function __get($property)
  {
    if (property_exists($this, $property)) {
      return $this->$property;
    }
  }

  public function __set($property, $value)
  {
    if (property_exists($this, $property)) {
      $this->$property = $value;
    }

    return $this;
  }

  function sendEmail()
  {
    $mail = new PHPMailer(true);

    $mail->isSMTP();
    //$mail->SMTPDebug = SMTP::DEBUG_SERVER; 
    $mail->Host       = self::MAILHOST;
    $mail->SMTPAuth   = true;
    $mail->Username   = self::MAILUSER;
    $mail->Password   = self::MAILPASS;
    $mail->SMTPSecure = 'ssl'; //PHPMailer::ENCRYPTION_SMTPS;
    $mail->Port       = 465;

    $mail->setFrom(self::MAILEMAIL, self::MAILNAME);
    $mail->addAddress($this->emailto, $this->name);

    $mail->isHTML(true);
    //$this->message = file_get_contents(__DIR__ . '/emails/mail.html');
    $this->message = "
        Congratulations, we have Auto Registered you for the upcoming e-CBM-3 Event.<br>
<br>
Please do join us on 7th Jan 2020 at 08.55 am by clicking the below link.<br>
<b>Link:</b> https://coact.live/eCBM3/<br>
<b>Note: Please use your official Email ID only to log in.</b> <br>
Once you are inside the lobby area, please enter ortho NE hall, Ortho SW Or Gynae hall as per your sales area.<br>
<br>
<br>
<b>For <u>ORTHO</u> ExCom Open house Session Join below meeting ID by 11:40 AM on 7th Jan</b><br>
https://us02web.zoom.us/j/88275771929?pwd=eE4yMmswQ3l5V2x6cFFmNG81WFNoQT09<br>
<b>Meeting ID: 882 7577 1929    ||    Passcode: 247914</b> <br>
<br>
<br>
<b>For <u>GYNAC</u> ExCom Open house Session Join below meeting ID by 02:25 PM on 7th Jan</b><br>
https://us02web.zoom.us/j/85955679521?pwd=eHpOaDlEMmJBTEtrVVR6TTNhZTBDdz09<br>
<b>Meeting ID: 859 5567 9521      ||    Passcode: 340227 </b><br>
<br>
In case of any technical issue please call Technical Support on +917314-855-655";

    $mail->Subject = $this->subject;
    $mail->MsgHTML($this->message);

    if ($mail->send()) {
      return true;
    } else {
      return false;
    }
  }
}
