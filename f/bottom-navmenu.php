<nav class="navbar bottom-nav">
  <ul class="nav mr-auto ml-auto">
    <li class="nav-item">
      <a class="nav-link" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a>
    </li>
    <li> <a class="show_attendees" href="#"><i class="fa fa-users">
          <div id="chat-message"></div>
        </i><span class="hide-menu">Attendees</span></a></li>
    <li class="nav-item">
      <a class="nav-link showpdf" href="assets/resources/confagenda.pdf" title=""><i class="far fa-list-alt"></i><span class="hide-menu">Agenda</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link show_talktous" data-from="<?php echo $_SESSION['userid']; ?>" href="#" title="Helpdesk"><i class="fa fa-info"></i><span class="hide-menu">Talk to Us</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link logout" href="logout.php" title="Logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </li>
  </ul>

</nav>
<div id="helpline">
  For assistance:<br>
  <i class="fas fa-phone-square-alt"></i> +917314-855-655
</div>