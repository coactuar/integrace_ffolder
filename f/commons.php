<div class="modal fade" id="attendees" tabindex="-1" role="dialog" aria-labelledby="attendeesLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="attendeesLabel">Attendees</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>
      <div class="modal-body p-1">
        <ul class="modal-tabs nav nav-tabs" role="tablist">
          <li id="listAttendees" class="active">
            <a href="#">Attendees</a>
          </li>
          <li id="chatInbox" class="">
            <a href="#">Chat Inbox</a>
          </li>

        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="attendeesList" style="display:block;">
            <div class="search-area">
              <form method="post">
                <div class="row">
                  <div class="col-12 col-md-6 pr-0">
                    <input type="text" id="attendee-search" placeholder="Search by name" class="input">
                  </div>
                  <div class="col-12 col-md-4 text-left">
                    <button type="submit" id="search-attendee" value="Search">Search</button><button type="submit" id="clear-search-attendee" value="Clear">Clear</button>
                  </div>
                  <div class="col-12 col-md-2 text-right">
                    <button type="button" id="refresh-attendees">Refresh</button>
                  </div>
                </div>

              </form>

            </div>
            <div id="attendeeList" class="content scroll"></div>
          </div>
          <div class="tab-pane" id="inboxChat" style="display:none;">
            <div id="attendees-list-chat" class="content scroll">

            </div>
          </div>

        </div>

      </div>

    </div>
  </div>
</div>

<!--Helpdesk-->
<?php
$file = $_SERVER["SCRIPT_NAME"];
$break = Explode('/', $file);
$pfile = $break[count($break) - 1];
?>
<div id="talktous" class="popup-dialog">
  <div class="popup-content">
    <div id="chat_team" class="team_chat_box">
      <div class="chat_history scroll" data-touser="team" id="chat_history_team"></div>
      <form>
        <div class="form-group">
          <input name="chat_message_team" id="chat_message_team" rows="1" class="input sendmsg" autocomplete="off">
        </div>
        <div class="form-group text-left">
          <button type="button" name="send_teamchat" class="send_teamchat btn btn-primary" data-src="<?php echo $pfile ?>" data-to="team" data-from="<?php echo $userid ?>">Send</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="attendees-chat"></div>